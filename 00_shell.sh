#!/bin/bash
set -e

DEPENDENCIES="curl git"
ZSH_PKG="zsh"

info() {
    echo '[INFO] ' "$@"
}

warn() {
    echo '[WARN] ' "$@" >&2
}

fatal() {
    echo '[ERROR] ' "$@" >&2
    exit 1
}

install_packages () {
    if [ -x "$(command -v apt)" ];
    then
        sudo apt-get update > /dev/null
        sudo apt-get install -y $1 > /dev/null 
    elif [ -x "$(command -v dnf)" ];
    then
        sudo dnf install -y $1 > /dev/null
    elif [ -x "$(command -v zypper)" ];
    then
        sudo zypper install -y $1 > /dev/null
    else
        fatal "FAILED TO INSTALL PACKAGE: Package manager not found. You must manually install: $1">&2;
    fi

    if [ $? -ne 0 ];
    then
        fatal "\"$1\" has not been installed"
    fi
}

install_dependencies () {
    info "Installing dependencies"
    for pkg in "$@"
    do 
        install_packages $pkg
    done
    info "Dependencies installed"
}

install_zsh () {
    info "Installing \"zsh\""
    install_packages ${ZSH_PKG}
    info "\"zsh\" installed"
}

kubectl () {
    info "Installing \"kubectl\""
    curl -LOs https://dl.k8s.io/release/`curl -LSs https://dl.k8s.io/release/stable.txt`/bin/linux/amd64/kubectl 1> /dev/null
    chmod +x ./kubectl
    sudo mv ./kubectl /usr/local/bin/kubectl
    info "\"kubectl\" has been installed"
}

copy_fonts () {
    info "Copying fonts"
    mkdir -p ~/.local/share/fonts
    cp -r NerdFonts ~/.local/share/fonts
}

on_my_zsh () {
    info "Installing \"Oh my zsh\""
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh 1> /dev/null)" "" --unattended 1> /dev/null
    sudo chsh artem -s /usr/bin/zsh
    
    info "Setting up \"zsh\""
    cp .zshrc ~/
}

zsh_plugins () {
    info "Installing zsh plugins"
    git clone https://github.com/johanhaleby/kubetail.git ~/.oh-my-zsh/custom/plugins/kubetail 1> /dev/null
}

starship () {
    info "Installing \"Starship\""
    curl -sS https://starship.rs/install.sh > install.sh
    sudo chmod +x install.sh && sudo ./install.sh -f

    info "Setting up \"Starship\""
    mkdir -p ~/.config && cp starship.toml ~/.config
}

{   
    install_dependencies ${DEPENDENCIES}
    install_zsh
    kubectl
    copy_fonts
    on_my_zsh
    zsh_plugins
    starship
    info "Finished"
    info "Logout and restart shell"
}


# ## Run Zsh
# echo "Starting Zsh..."
# cp .zshrc ~/
# zsh
# source ~/.zshrc
